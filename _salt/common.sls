passwd: pkg.installed

zsh:
  pkg.installed

sudo:
  pkg.installed

mjolnir:
  group.present

/etc/sudoers.d/mjolnir:
  file.managed:
    - require:
      - pkg: sudo
      - group: mjolnir
    - contents: |
        %mjolnir ALL=(ALL) NOPASSWD:ALL
