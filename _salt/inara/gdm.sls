/etc/gdm3/greeter.dconf-defaults:
  file.managed:
    - source: salt://inara/greeter.dconf-defaults
    - listen_in:
      - service: gdm

gdm:
  service.enabled
