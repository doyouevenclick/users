include:
- common

astraluma:
  group.present: []
  user.present:
  - fullname: Jamie Bliss
  - usergroup: true
  - home: /home/astraluma
  - groups:
    - mjolnir
  - optional_groups:
    - adm
    - salt
    - sudo
    - wheel
    - admin
    - wemux
    - plugdev
    - www-data
{% if grains['id'] == 'inara' %}
    - libvirt
{% endif %}
  - shell: /bin/zsh
  - require:
    - pkg: zsh
    - group: astraluma
    - group: mjolnir

/home/astraluma/.ssh:
  file.directory:
    - user: astraluma
    - group: astraluma
    - mode: 700
    - require:
      - user: astraluma

"AAAAC3NzaC1lZDI1NTE5AAAAIAdl0DU6DcVFpT00eK9+ujSQuWaxBaP7Z5BHrhD6PIay":
  ssh_auth.present:
    - user: astraluma
    - enc: ed25519
    - comment: cayde7
    - require:
      - file: /home/astraluma/.ssh

"AAAAC3NzaC1lZDI1NTE5AAAAINEgcvdotsdIAp0s6uqQI/p+EYUAUVscL2w5K1cmJDG6":
  ssh_auth.present:
    - user: astraluma
    - enc: ed25519
    - comment: "cardno:000612285692"
    - require:
      - file: /home/astraluma/.ssh

us_locale:
  locale.present:
    - name: en_US.UTF-8

{% if salt['file.file_exists']('/usr/local/bin/xonsh') %}
astraluma-xonsh-venv:
  cmd.run:
    - name: "xonsh -c 'xontrib load xonshzip; home-environ-setup'"
    - creates: /home/astraluma/.local/xonsh

{% if grains['id'] not in ('cayde7', 'inara') %}
/home/astraluma/.xonshrc:
  file.managed:
    - contents: |
        xontrib load xonshzip prompt_ret_code
{% endif %}
{% endif %}
