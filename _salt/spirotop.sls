prod:
  master:
    - master
  '*':
    - astraluma
    - nicoo
    - agmlego

  inara: 
  - desktop.minikube
  - desktop.docker
  - desktop.pulumi
  - inara.gdm

  cayde7:
  - desktop.kubectl
