# -*- mode: salt -*-
include:
- common

nicoo:
  group.present: []
  user.present:
  - usergroup: true
  - home: /home/nicoo
  - groups:
    - mjolnir
  - optional_groups:
    - adm
    - salt
    - sudo
    - wheel
    - admin
    - wemux
  - shell: /bin/zsh
  - require:
    - pkg: zsh
    - group: nicoo
    - group: mjolnir

/home/nicoo/.ssh:
  file.directory:
    - user: nicoo
    - group: nicoo
    - mode: 700
    - require:
      - user: nicoo

"AAAAC3NzaC1lZDI1NTE5AAAAIEPefie/fg02FIqUJ+wgEe9OmECEcfyW6EPKo4x1AWfK":
  ssh_auth.present:
    - user: nicoo
    - enc: ed25519
    - comment: neon
    - require:
      - file: /home/nicoo/.ssh

"AAAAC3NzaC1lZDI1NTE5AAAAICm2hQjb7XmAHq1ZvUyVu9kRcoTrNKppGWTpFDnTCPZj":
  ssh_auth.present:
    - user: nicoo
    - enc: ed25519
    - comment: xenon
    - require:
      - file: /home/nicoo/.ssh

"AAAAB3NzaC1yc2EAAAADAQABAAACAQCwF3hn3gSQR8tCb3Kk49GZ+zw7Usnvhy2yGm17mZx9Q37S2cVaRmHv4a5vIktq3x5ToZ0aD4CbuXQ2ln5Q1kh0bkAAp8udLL5m2MbdUv3G+pgr+L5yFnfCelNVpbq0qUBNODgvPaAdGiv33ChKmBoZ42tsU6sgULG2ohkQ7ePutsenBQqtcxeIdZgv6C+ZuDFY3MmX8sndA7tX84DIhC2NlPJocd0HKufzgNDqg8dQQEmiNLRs6jncry4aVAawRgIutA9uls/5UNKpdMh7i2TXLMQOLqhUyMtE6dj8ZdY9vmipE0lZWdLuoOxroUIeoMxbAt3M94zugoggyDZOI/JpfbyHDQbBGOypf+8e3+bVw1boDvPctYa1ZPrLupD0jvpgCOcDD5zczZOCkxMCLveiKL6UBZW/GBIANBMWZkvMVAz61Ge6Mlz2tOKZh7FoIJz5xoeBDePI/iiGcZTiJ1XrigJQ5TKKHfykg06r/rZ5Sxl9SiES9HY6WUOSgQAHGmNr8QbfOCGzlM3qmPmW9okiDdHIt9RwUxEyP6Kjz66XJ3ZLXFqaokUcuftoP1kgtLk3NasT6/usqUwQmDhLp8u2Ro4yi/SCl6+ZXvYS/vK3cl4vJUiTVZ4UlAyOifMrHEE3Zdydin+crYE9IuQZ/imlLF38RjtCg2AZfanhOj1wJw==":
  ssh_auth.present:
    - user: nicoo
    - enc: rsa
    - comment: smartcard
    - require:
      - file: /home/nicoo/.ssh

git:
  pkg.installed

kitty-terminfo:
  pkg.installed

# TODO: Check repository signature
https://github.com/nbraud/.dotfiles:
  git.latest:
    - target: /home/nicoo/.dotfiles
    - user: nicoo
    - submodules: True
    - require:
        - user: nicoo
        - pkg: git

stow:
  pkg.installed

stow -R emacs git gnupg zsh:
  cmd.run:
    - runas: nicoo
    - cwd: /home/nicoo/.dotfiles
    - require:
        - git: https://github.com/nbraud/.dotfiles
        - pkg: stow
    - onchanges:
        - git: https://github.com/nbraud/.dotfiles
