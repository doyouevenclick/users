include:
- common

agmlego:
  group.present: []
  user.present:
  - fullname: Amelia Meyer
  - usergroup: true
  - home: /home/agmlego
  - groups:
    - mjolnir
  - optional_groups:
    - adm
    - salt
    - sudo
    - wheel
    - admin
    - plugdev
{% if grains['id'] == 'brigid' %}
    - www-data
    - creekfleet
    - btsync
{% endif %}
  - shell: /usr/local/bin/xonsh
  - require:
    - group: agmlego
    - group: mjolnir

/home/agmlego/.ssh:
  file.directory:
    - user: agmlego
    - group: agmlego
    - mode: 700
    - require:
      - user: agmlego

"AAAAC3NzaC1lZDI1NTE5AAAAIDF2jQHtDMUz84Ul644Nf6nqeI7tedN0eRmjys8JrSgK":
  ssh_auth.present:
    - user: agmlego
    - enc: ed25519
    - comment: inanna
    - require:
      - file: /home/agmlego/.ssh

"AAAAC3NzaC1lZDI1NTE5AAAAIIxULzQqLX7m9IEiWh/EdejyNi+i5i0sTVaXW2wdhafl":
  ssh_auth.present:
    - user: agmlego
    - enc: ed25519
    - comment: enedina
    - require:
      - file: /home/agmlego/.ssh

"AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAF1VHoPjBIMgdWLNF0UfK2ndSTfw02EFviiikT468mHJlEGQlYR+GDx3n5ffSM95DaHlg0tp2GOzw8abt9QxIYdrQHMfePKMiLfHATL6wJN5uwKRd3O1cggfXU6HZqTltleeBXidk6F8hQyhL9rd1I/r1Oxj6HQdyB2vVICxjoQi7or4Q==":
  ssh_auth.present:
    - user: agmlego
    - enc: ecdsa-sha2-nistp521
    - comment: juicessh@oneplus7t
    - require:
      - file: /home/agmlego/.ssh

{% if salt['file.file_exists']('/usr/local/bin/xonsh') %}
agmlego-xonsh-venv:
  cmd.run:
    - name: "xonsh -c 'xontrib load xonshzip; home-environ-setup'"
    - creates: /home/agmlego/.local/xonsh

{% endif %}
