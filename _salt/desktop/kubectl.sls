{% if grains['os_family'] == 'Debian' %}
kubernetes-repo:
  pkgrepo.managed:
    - name: 'deb https://apt.kubernetes.io/ kubernetes-stretch main'
    - file: /etc/apt/sources.list.d/kubernetes.list
    - key_url: https://packages.cloud.google.com/apt/doc/apt-key.gpg

kubectl: pkg.installed
{% endif %}
