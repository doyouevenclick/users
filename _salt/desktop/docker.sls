{% if grains['os_family'] == 'Debian' %}
docker-repo:
  pkgrepo.absent:
    - name: 'deb [arch=amd64] https://download.docker.com/linux/debian {{grains['oscodename']}} stable'

docker-ce: pkg.removed
docker-ce-cli: pkg.removed
containerd.io: pkg.removed

docker.io: pkg.installed
{% endif %}
