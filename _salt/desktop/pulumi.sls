#!pyobjects

if grains('osarch') == 'amd64':
    for fname in (
        'pulumi', 'pulumi-language-go', 'pulumi-language-nodejs',
        'pulumi-language-python', 'pulumi-language-python-exec',
        'pulumi-resource-pulumi-nodejs',
    ):
        File.managed(
            '/usr/local/bin/' + fname,
            source='salt://_artifacts/pulumi/' + fname,
            mode='0755',
        )
