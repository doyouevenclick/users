include:
- common.python

git: pkg.installed
vim: pkg.installed
curl: pkg.installed
dnsutils: pkg.installed
nmap: pkg.installed
tor: pkg.installed

pipenv:
  pip.installed:
    - bin_env: /usr/bin/pip3
