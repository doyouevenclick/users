{% if grains['os'] == 'Debian' %}
slack-repo:
  pkgrepo.managed:
    - name: 'deb https://packagecloud.io/slacktechnologies/slack/debian/ jessie main'
    - file: /etc/apt/sources.list.d/slack.list
    - key_url: https://packagecloud.io/slacktechnologies/slack/gpgkey

/etc/default/slack:
  file.managed:
    - contents: ''

slack-desktop: pkg.installed
{% endif %}

