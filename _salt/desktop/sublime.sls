{% if grains['os'] == 'Debian' %}
sublime-repo:
  pkgrepo.managed:
    - name: 'deb https://download.sublimetext.com/ apt/stable/'
    - file: /etc/apt/sources.list.d/sublime-text.list
    - key_url: https://download.sublimetext.com/sublimehq-pub.gpg

sublime-text: pkg.installed
{% endif %}

