include:
- desktop.kubectl

{% if grains['os_family'] == 'Debian' %}
libvirt-clients: pkg.installed
libvirt-daemon-system: pkg.installed
qemu-kvm: pkg.installed

libvirtd:
  service.running:
    - enable: true
    - requires:
      - pkg: libvirt-daemon-system
{% endif %}

{% if grains['osarch'] == 'amd64' %}
/usr/local/bin/docker-machine-driver-kvm2:
  file.managed:
    - source: https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2
    - source_hash: 0abd2ea071dc411e2a20305dcce596a26a788d8f9b041d2906474869d29016b01c00aa63c8223d44c2a55adeb7adf238fe77fd7a8b6dea764116d4d2d247201b
    - mode: 0755

/usr/local/bin/minikube:
  file.managed:
    - source: https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    - source_hash: dfd978a8da7bd73f58b3cd94649adfe6d6e078aefe1e74139481ddb156141f3dc054bd06d2a90f320dbd1d39e05528baedf88a18eecfecbfdf49e669dd5275f7
    - mode: 0755

"minikube config set vm-driver kvm2":
  cmd.run:
    - watch:
      - file: /usr/local/bin/minikube
      - file: /usr/local/bin/docker-machine-driver-kvm2
{% endif %}
